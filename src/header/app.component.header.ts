import { Component, OnInit,Output,EventEmitter,Input } from '@angular/core';

@Component({
    selector: 'header',
    templateUrl: 'app.component.header.html',
    styleUrls: ['./app.component.header.css']
})

export class HeaderComponent implements OnInit {
    @Output() notify: EventEmitter<void> = new EventEmitter<void>();
    @Input() Opend:boolean;
    constructor() { }

    ngOnInit() { }
    sidebarClicked(){
        this.notify.emit();
    }
}