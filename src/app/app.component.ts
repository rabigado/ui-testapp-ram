import {Component, trigger, state, style, transition, animate} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = `Hello, Dave. You're looking well today.`;
  isOpen=true;

  onNotify():void {
    this.isOpen=!this.isOpen;
  }
}
