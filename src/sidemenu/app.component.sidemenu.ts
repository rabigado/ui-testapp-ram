import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './app.component.sidemenu.html',
  styleUrls: ['./app.component.sidemenu.css']
})
export class MenuComponent implements OnInit {
  private selectedItem: any;
  private side_menu_items = [{name:'Home',icon:'home'},{name:'Workflow',icon:'Workflow'},{name:'Statistics',icon:'Statistics'},
  {name:'Calendar',icon:'Calendar'},{name:'Users',icon:'Users'},{name:'Settings',icon:'Settings'}];
  constructor() { 
    this.selectedItem = this.side_menu_items[0];
  }

  ngOnInit() {
  }
  listClick(event, newValue) {
    this.selectedItem = newValue; 
    
  }
}