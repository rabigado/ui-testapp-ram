import { Component, OnInit,Input } from '@angular/core';

@Component({
    selector: 'stacklist-item',
    styleUrls: ['./app.component.stacklist.css'],
    template:`<li class="list_item_wrapper">
    <div class="aside aside-1">
            <div *ngIf="item.Label" class="numberCircle blue">{{item.Label}}</div>
            <div *ngIf="item.From" class="round_image">
                    <img class="img-circle" src="../saitama.png">
            </div>
    </div>
    <div class="main">
            <div *ngIf="item.Message" class="bigText">{{item.Message}}</div>
            <div *ngIf="item.Name==='M'">
                <span class="headerText">{{item.From.FullName}}</span>
                <span class="greyText">{{pipeTimeString(item.Date,true)}}</span>
            </div>
            <div *ngIf="item.Body" class="bodyText">{{turncateString(item.Body)}}</div>
            <div *ngIf="item.Name === 'A'" class="activityText">
                <span class="headerText">{{item.From.FullName}}</span>
                <span class="listBottomMenu">{{item.Action}}</span>
                <span class="headerText">{{item.At}}</span>
            </div>
            <div *ngIf="item.due || item.When" class="timeText">
                <div class="floatleft sprite Clock"></div>
                <span *ngIf="item.due">{{pipeTimeString(item.due,false)}}</span>
                <span *ngIf="item.When">{{pipeTimeString(item.When,true)}}</span>
            </div>
            <div *ngIf="item.Body" class="listBottomMenu">
                    <div class='sprite Options_small floatleft'></div>
                    <div class='sprite BackArrow floatleft'></div>
            </div>
    </div>
    <div *ngIf="item.Name === 'T'" class="aside aside-2">
            <div class="dots_menu" (click)="showDotsMenu()"></div>
            <div [hidden]="showHide" id="myDropdown" class="dropdown-content">
                    <a href="#" (click)="showDotsMenu()">Action 1</a>
                    <a href="#" (click)="showDotsMenu()">Action 2</a>
                    <a href="#" (click)="showDotsMenu()">Action 3</a>
            </div>
    </div>
</li>`
})

export class StackListItem implements OnInit {
    @Input() item:any;
    showHide:boolean = true;
    constructor() { }
    showDotsMenu(){
        this.showHide = !this.showHide;
        return this.showHide;
    }
    turncateString(str){
        return str.length > 30? str.substring(0,30)+'(...)':str; 
    }

    pipeTimeString(value,isMessage){
        var result:string;
        // current time
        let now = new Date().getTime();

        // time since message was sent in seconds
        let delta = Math.abs((now - value) / 1000);
        let text  = (isMessage ? "ago": (value > now ? "left" : "delays"))
        // format string
        if (delta < 10)
        {
            result = 'A few moments ago';
        }
        else if (delta < 60)
        { // sent in last minute
            result = 'About ' + Math.floor(delta) + ' Seconds '+ text;
        }
        else if (delta < 3600)
        { // sent in last hour
            result = 'About ' + Math.floor(delta / 60) + ' Minutes '+ text;
        }
        else if (delta < 86400)
        { // sent on last day
            result = 'About ' + Math.floor(delta / 3600) + ' Hours '+ text;
        }
        else
        { // sent more than one day ago
            result = 'About ' + Math.floor(delta / 86400) + ' Days '+ text;
        }
        return result;
    }
    ngOnInit() { }
}