import { Component, OnInit,Input } from '@angular/core';
import {MockApiService} from '../assets/MockDataApi';

@Component({
    selector: 'mainbody',
    templateUrl: './app.component.mainbody.html',
    styleUrls: ['./app.component.mainbody.css']
})
export class MainBodyComponent implements OnInit {
    @Input() title: string;
    data:any;
    
    constructor(private mockDataService:MockApiService) {}

    ngOnInit() { 
        this.mockDataService.get().subscribe(data => this.data = data)
    }
}