import { UITestAppPage } from './app.po';

describe('ui-test-app App', function() {
  let page: UITestAppPage;

  beforeEach(() => {
    page = new UITestAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
