import { Component, OnInit,Input } from '@angular/core';

@Component({
    selector: 'stack-list',
    templateUrl: './app.component.stacklist.html',
    styleUrls: ['./app.component.stacklist.css']
})
export class StackListComponent implements OnInit {
    @Input() title?: string;
    @Input() data?:any;
    @Input() blue?:number;
    @Input() red?:number;
    showHide:boolean = true;
    constructor() { }

    ngOnInit() { }
   
}