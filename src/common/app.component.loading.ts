import { Component, OnInit,Input } from '@angular/core';

@Component({
    selector: 'loading',
    templateUrl: './app.component.loading.html',
    styleUrls: ['./app.component.loading.css']
})
export class LoadingComponent implements OnInit {
    @Input() showLoading: boolean = true;
    constructor() { }

    ngOnInit() { }
}