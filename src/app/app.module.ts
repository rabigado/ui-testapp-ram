import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MenuComponent } from '../sidemenu/app.component.sidemenu';
import {MainBodyComponent} from '../mainbody/app.component.mainbody';
import {StackListComponent} from '../stacklist/app.component.stacklist';
import {HeaderComponent} from '../header/app.component.header';
import {StackListItem} from '../stacklist/stacklist.component.item';
import {MockApiService} from '../assets/MockDataApi';
import {LoadingComponent} from '../common/app.component.loading'
//stacklist-item
@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    MainBodyComponent,
    StackListComponent,
    HeaderComponent,
    StackListItem,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [MockApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
