import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/RX';
@Injectable()
export class MockApiService {
    get() {
        let subject = new Subject();
        setTimeout(function() {
            subject.next(MockData);
            subject.complete();
        }, 400+Math.random()*1000);
        return subject;
    }
  }

  const MockData:any = 
  {
    Tasks:{Name:"Tasks",Number1:1,Number2:3,
        data:[{Name:'T',Label:"L",Message:"Task 1",due:"1504643183966"},
        {Name:'T',Label:"L",Message:"Task 2",due:"1504643183966"},
        {Name:'T',Label:"L",Message:"Task 3",due:"1504643183966"},
        {Name:'T',Label:"N",Message:"Take Over The World",due:"1505293374937"}
    ]},
    Messages:{Name:"Messages",Number1:1,Number2:4,
        data:[{Name:'M',From:{FullName:'ygritte',Img:''},Date:'1504643183966',Body:"you know nothing"},
                {Name:'M',From:{FullName:'Ned Stark',Img:''},Date:'1504643183966',Body:"One doesn't simply walk into mordor"},
                {Name:'M',From:{FullName:'Khaleesi ',Img:''},Date:'1504643183966',Body:"I'm Daenerys Stormborn of the House Targaryen, First of Her Name, the Unburnt, Queen of the Andals and the First Men, Khaleesi of the Great Grass Sea, Breaker of Chains, and Mother of Dragons."},
                {Name:'M',From:{FullName:'Little finger',Img:''},Date:'1504643183966',Body:"chaos is a ladder"}
            ]},
    Activity:{Name:"Activity",Number1:1,Number2:4,
        data:[{Name:'A',From:{FullName:'Scott Jenson',Img:''},Action:'doing UI design',At:"Google's mobile UX",When:'1504643183966'},
        {Name:'A',From:{FullName:'Ethan Marcotte',Img:''},Action:'started that whole',At:"responsive web design' thing",When:1504643183966},
        {Name:'A',From:{FullName:'Dennis Ritchie',Img:''},Action:'Growing an awesome beard',At:'progrmmers heaven',When:'1504643183966'},
        {Name:'A',From:{FullName:'Ryan Dahl',Img:''},Action:'make javascript magic',At:'with callback hell',When:'1504643183966'}
    ]}
};